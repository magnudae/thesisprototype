package com.magnudae.matching;

import com.hp.hpl.jena.util.iterator.Map1;
import com.magnudae.controllers.FormBean;
import com.magnudae.utils.Namespaces;
import com.magnudae.utils.Settings;
import com.magnudae.utils.QueryBuilder;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;

import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.util.FileManager;

public class CarData {
    /**
     * Ser ut som at du må lage en klasse Car eller COCar for å holde på lex-stringen og modelnavnet.
     * Deretter ha noen datastrukturere som holder på denen bilen og den linkedConf man har kommet til
     * ganske omfattende programmeringsjobb. Må begynner på neste uke.
     * Deretter bestemme seg for hvordan å spørre en model om flere ting. Kan være simpelt, men bruker lang tid.
     */
    Map<String, PartialCar> renaultModels;
    Map<String, Map<String, PartialCar>> daimlerModels;
    Map<String, Thread> threadPool;
    public boolean findOneSpec = false;
    public boolean vso = true;
    public boolean co = true;
    boolean highPrecision = true;
    public CarData(String option, boolean highPrecision){
        initateData(option);
        this.highPrecision = highPrecision;
    }

    public void setHighPrecision(boolean precision){
        highPrecision = precision;
    }
    public void enableOntologies(String ontology){
        if(ontology.equals("-vso")){
            renaultModels = new HashMap<String, PartialCar>();
            co = false;
            vso = true;
        }
        else if (ontology.equals("-co")){
            vso = false;
            co = true;
            daimlerModels = new HashMap<String, Map<String, PartialCar>>();
        }else{
            co = true;
            vso = true;
        }
    }

    public void initateData(String option){
        renaultModels = new HashMap<String, PartialCar>();
        daimlerModels = new HashMap<String, Map<String, PartialCar>>();
        threadPool = new HashMap<String, Thread>();
        if(option.equals("-vso")){
            inititateVSOData();
            co = false;
        }
        else if (option.equals("-co")){
            inititateCOData();
            vso = false;
        }else{
            inititateCOData();
            inititateVSOData();
        }
    }
    private void inititateCOData(){
        Settings.initModelNames();
        Settings.initLexicons();

        for(String lex: Settings.renaultLexicons.keySet()){
            PartialCar newCar = new PartialCar(Settings.renaultModelNames.get(lex), lex, null);
            newCar.setLinkedConf(Settings.renaultStartingPoint);
            renaultModels.put(lex, newCar);

        }
    }
    private void inititateVSOData(){
        for(int i = 0; i < Settings.daimlerLexicons.length; i++){
            Model model = FileManager.get().loadModel(Settings.daimlerLexicons[i]);
            daimlerModels.put(Settings.daimlerLexicons[i], addPossibleModels(model));
        }
    }

    /*
     * Function made for making benchmarking faster
     */
    public void resetCarData(){
        for(Map<String, PartialCar> map : daimlerModels.values()){
            for(PartialCar pc : map.values()){
                pc.getValidSpec().clear();
            }
        }
        for(PartialCar pc : renaultModels.values()){
            pc.getValidSpec().clear();
            pc.setLinkedConf(Settings.renaultStartingPoint);
        }
    }
    /*
     * Adding models to renaultModels map
     * For VSO
     */
    public Map<String, PartialCar> addPossibleModels(Model model){
        Map<String, PartialCar> tmp = new HashMap<String, PartialCar>();
        String sparqlQuery = Settings.SPARQLPREFIX +
                "SELECT ?modelURI ?modelName WHERE { ?modelURI a coo:Derivative ." +
                "									   ?modelURI rdfs:label ?modelName}";
        Query query = QueryFactory.create(sparqlQuery);
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        ResultSet rs = qe.execSelect();
        while(rs.hasNext()){
            QuerySolution qs = rs.next();
            String modelURI = qs.get("modelURI").toString();
            tmp.put(modelURI, new PartialCar(qs.getLiteral("modelName").toString(), modelURI,  model));
        }
        return tmp;
    }

    /*
     * Starting to search through possible models.
     * Searches through the two different datasets for each while
     * For both CO and VSO
     */
    public void searchThroughModels(FormBean formBean){

        long i = System.currentTimeMillis();
        ResultSet results;
        if(vso){
            int models = 0;
            for(Map<String, PartialCar> map : daimlerModels.values()){
                models += map.size();
            }
            CountDownLatch cdl = new CountDownLatch(models);
            for(String vsoModel: daimlerModels.keySet()){
                doVsoSearch(formBean, vsoModel, cdl);
            }
            try{
                cdl.await();
            }catch(InterruptedException exception){
                exception.printStackTrace();
                System.exit(1);
            }
            System.out.println("VSO time: " + (System.currentTimeMillis() - i));
        }
        i = System.currentTimeMillis();
        /*
            With the data representation in VSO one can use more of the SPARQL functionality to
            narrow down the search result. This is because with VSO the user is almost
            forced to add what kind of literal type the values are.
            In CO they are just represented as rdfs:labels. Which means that we do not know
            if they are of int, float or anything else. That means that we have to "assume"
            what to check with after the SPARQL query is done.
         */
        if(co){
            Map<String, PartialCar> tmp = new HashMap<String, PartialCar>();
            CountDownLatch cdl = new CountDownLatch(renaultModels.values().size());
            for(String key : renaultModels.keySet()){
                PartialCar pc = renaultModels.get(key);
                Model thisModel = FileManager.get().loadModel(Settings.renaultLexicons.get(key));
                COCarFinalizer coThread = new COCarFinalizer(formBean.getValues(), pc, thisModel, cdl);
                coThread.start();
            }

            try{
                cdl.await();
            }catch(InterruptedException exception){
                exception.printStackTrace();
                System.exit(1);
            }
            System.out.println("CO time: " + (System.currentTimeMillis() - i));

        }

    }


    public void doVsoSearch(FormBean formBean, String dataModel, CountDownLatch cdl){
        Model model = FileManager.get().loadModel(dataModel);
        model.add(FileManager.get().loadModel(Settings.compatibilityCOO));
        model.add(FileManager.get().loadModel(Settings.localCOO));
        InfModel inferredModel = ModelFactory.createInfModel(ReasonerRegistry.getOWLReasoner(), model);
        Map<String, PartialCar> currentModels = daimlerModels.get(dataModel);
        //CountDownLatch cdl = new CountDownLatch(currentModels.values().size());
        for(PartialCar pc : currentModels.values()){
            VSOCarFinalizer vsoThread = new VSOCarFinalizer(formBean.getValues(), pc, inferredModel, cdl);
            vsoThread.start();
        }


    }
    abstract class CarFinalizer extends Thread {
        Map<String, Value> form;
        PartialCar car;
        CountDownLatch countDown;
        Map<String, String> valuesSoFar;


        public CarFinalizer(Map<String, Value> form, PartialCar car, CountDownLatch countDown){
            this.form = form;
            this.car = car;
            this.countDown = countDown;
            valuesSoFar = new HashMap<String, String>();
        }

        public void run(){
            String[] keys = new String[form.keySet().size()];
            keys = form.keySet().toArray(keys);
            recursiveCompatibilityCheck(keys, 0);
            if(car.getValidSpec().isEmpty() || !findOneSpec){
                countDown.countDown();
            }
        }

        abstract void recursiveCompatibilityCheck(String[] keys, int counter);


    }
    class VSOCarFinalizer extends CarFinalizer {
        InfModel inferredModel;
        List<String> specsSoFar;
        public VSOCarFinalizer(Map<String, Value> form, PartialCar car, InfModel inferredModel, CountDownLatch countDown){
            super(form, car, countDown);
            this.inferredModel = inferredModel;
            specsSoFar = new LinkedList<String>();
        }

        void recursiveCompatibilityCheck(String[] keys, int counter){
            if(!(counter < keys.length)){
                Map<String, String> finalizedSpecs = new HashMap<String, String>();
                finalizedSpecs.putAll(valuesSoFar);
                car.setValidSpec(finalizedSpecs);
                if(findOneSpec)
                    countDown.countDown();
                return;
            }
            Value wantedValue = form.get(keys[counter]);
            if(!wantedValue.isEmpty()){
                ResultSet results = executeQuery(inferredModel, QueryBuilder.buildQueryForOneSpecificationVSOAndCOO(keys[counter], car.getLexRef(), specsSoFar, precision));
                while(results.hasNext()){
                    QuerySolution qs = results.next();
                    String value = qs.getLiteral("vsoValue").getString();
                    if(wantedValue.isValid(value)){
                        String specURI = qs.get("specification").toString();
                        specsSoFar.add(specURI);
                        valuesSoFar.put(keys[counter], value);
                        recursiveCompatibilityCheck(keys, ++counter);
                        --counter;
                        valuesSoFar.remove(keys[counter]);
                        specsSoFar.remove(specURI);
                    }
                }
            }else{
                recursiveCompatibilityCheck(keys, ++counter);
                --counter;
            }
        }

    }

    class COCarFinalizer extends CarFinalizer {
        Model lexicon;
        String[] linkedConfigurations;
        public COCarFinalizer(Map<String, Value> form, PartialCar car, Model lexicon, CountDownLatch cdl){
            super(form, car, cdl);
            this.lexicon = lexicon;
            linkedConfigurations = new String[form.keySet().size()];
        }

        void recursiveCompatibilityCheck(String[] keys, int counter){
            if(counter == 0) moveLinkedConfiguration(null, car.getLexRef()); //Init linked Configuration

            if(!(counter < keys.length)){
                Map<String, String> finalizedSpecs = new HashMap<String, String>();
                finalizedSpecs.putAll(valuesSoFar);
                car.setValidSpec(finalizedSpecs);
                if(findOneSpec)
                    countDown.countDown();
                return;
            }
            linkedConfigurations[counter] = car.getLinkedConf();

            Value wantedValue = form.get(keys[counter]);
            if(!wantedValue.isEmpty()){
                String sparqlQuery = QueryBuilder.buildQueryForOneSpecification(keys[counter]);
                ResultSet results = executeQuery(lexicon, sparqlQuery);
                while(results.hasNext()){
                    QuerySolution sol = results.next();
                    String literalValue = sol.getLiteral("value").getString().split("/")[0];
                    if(literalValue.contains("(")){
                        literalValue = literalValue.split("\\(")[1];
                        literalValue = literalValue.split("\\)")[0];
                    }
                    literalValue = literalValue.replace(",", ".");
                    if(!checkForCOMissingValues(literalValue) && wantedValue.isValid(literalValue)){
                        boolean isValid = true;
                        if(precision)
                            isValid = moveLinkedConfiguration(sol, car.getLexRef());
                        if(isValid){
                            valuesSoFar.put(keys[counter], literalValue);
                            recursiveCompatibilityCheck(keys, ++counter);
                            --counter;
                            car.setLinkedConf(linkedConfigurations[counter]);
                            valuesSoFar.remove(keys[counter]);
                        }
                    }
                }
            }else{
                recursiveCompatibilityCheck(keys, ++counter);
                --counter;
            }

        }

        boolean moveLinkedConfiguration(QuerySolution solution, String lexKey){
            Model linked;
            String sparqlQuery = "";
            if(car.getLinkedConf().equals(Settings.renaultStartingPoint)){ // For the first spec to set the correct linked configuration
                linked = FileManager.get().loadModel(Settings.renaultStartingPoint);
                sparqlQuery = Settings.SPARQLPREFIX +
                        "SELECT ?linkedConf WHERE { ?linkedConf co:lexicon <"+ lexKey +"> .}";
            }else{ // For the rest of the specs ++ same as the if
                linked = FileManager.get().loadModel(car.getLinkedConf());
                sparqlQuery = Settings.SPARQLPREFIX +
                        "SELECT ?linkedConf WHERE { ?model co:possible ?spec . " +
                        "?spec co:linkedConf ?linkedConf . " +
                        " ?spec co:specToBeAdded <" + solution.get("specification") +"> . }";

            }
            Query query = QueryFactory.create(sparqlQuery);
            QueryExecution qe = QueryExecutionFactory.create(query, linked);
            ResultSet rs = qe.execSelect();
            String l = car.getLinkedConf();
            while(rs.hasNext()){
                QuerySolution qs = rs.next();
                car.setLinkedConf(qs.get("linkedConf").toString());
                return true;
            }
            return false;
        }
    }



    public boolean checkForCOMissingValues(String literal){
        if(literal.equals("-")) return true;
        else if(literal.equals("NC")) return true;
        else if(literal.equals("tbc")) return true;
        else return false;
    }


    public ResultSet executeQuery(Model model, String sparqlQuery){
        Query query = QueryFactory.create(sparqlQuery);
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        ResultSet rs = qe.execSelect();
        return rs;
    }


    /*
     * Method for searching for possible specification
     * For both CO and VSO
     */
    public ResultSet doSpecSearch(Model lex, String specId){
        String sparqlQuery = Settings.SPARQLPREFIX +
                "SELECT ?value ?valuelbl ?model WHERE { ?var co:confVarId \"" + specId + "\" ; " +
                "							   co:hasValue ?value . " +
                "						OPTIONAL { ?value rdfs:label ?valuelbl .} " +
                "						OPTIONAL { ?model ?c ?var .} }";



        Query query = QueryFactory.create(sparqlQuery);
        QueryExecution qe = QueryExecutionFactory.create(query, lex);
        ResultSet rs = qe.execSelect();
        return rs;
    }

    /*
     * Printing current possible models
     */
    public void printCurrentModels(){
        for(PartialCar pc : renaultModels.values()){
            System.out.println(pc.getModelName());
        }
        for(Map<String, PartialCar> map : daimlerModels.values()){
            for(PartialCar  pc : map.values())
                System.out.println(pc.getModelName());
        }
    }

    public List<String> getCurrentModels(){
        List<String> li = new LinkedList<String>();
        for(Map<String, PartialCar> map : daimlerModels.values()){
            for(PartialCar pc : map.values()){
                if(!pc.getValidSpec().isEmpty())
                    li.add(pc.getModelName());
            }
        }
        for(PartialCar pc : renaultModels.values()){
            if(!pc.getValidSpec().isEmpty()){
                li.add(pc.getModelName());
            }
        }
        return li;
    }

    private String[] getKeys(){
        String[] keys = new String[9];
        keys[0] = Namespaces.WEIGHTTOTAL;
        keys[1] = Namespaces.EMISSION;
        keys[2] = Namespaces.FUELTANKVOLUME;
        keys[3] = Namespaces.WEIGHT;
        keys[4] = Namespaces.FUELCONSUMPTION;
        keys[5] = Namespaces.GEARSTOTAL;
        keys[6] = Namespaces.SPEED;
        keys[7] = Namespaces.SEATS;
        keys[8] = Namespaces.DOORS;

        return keys;
    }

    private String[] getKeysForQualitative(){
        String[] keys = new String[2];
        keys[0] = Namespaces.FUELTYPE;
        keys[1] = Namespaces.TRANSMISSION;
        return keys;
    }
    public void calculateValuesCO(){
        PrintWriter os = null;
        try{
            os = new PrintWriter(new File("COValues.txt"));
        }catch(Exception e){
            System.out.println("FAULT");
            e.printStackTrace();
            System.exit(1);
        }
        String[] keys = getKeys();
        String[] qualitativeKeys = getKeysForQualitative();
        for(String key : renaultModels.keySet()){
            PartialCar pc = renaultModels.get(key);
            Model thisModel = FileManager.get().loadModel(Settings.renaultLexicons.get(key));
            os.println("Car modelname: " + pc.getModelName());
            for(int k = 0; k < keys.length; k++){
                os.println("ID: " + keys[k]);
                String query = QueryBuilder.buildQueryForOneSpecification(keys[k]);
                ResultSet results = executeQuery(thisModel, query);
                double nmbr = 0;
                int cnt = 0;
                ArrayList<Double> arrayList = new ArrayList<Double>();
                while(results.hasNext()){
                    QuerySolution sol = results.next();
                    String literalValue = sol.getLiteral("value").getString().split("/")[0];
                    if(literalValue.contains("(")){
                        literalValue = literalValue.split("\\(")[1];
                        literalValue = literalValue.split("\\)")[0];
                    }
                    literalValue = literalValue.replace(",", ".");
                    if(!checkForCOMissingValues(literalValue)){
                        cnt++;
                        nmbr += Double.parseDouble(literalValue);
                        arrayList.add(Double.parseDouble(literalValue));
                    }
                }
                os.println("Average value: " + nmbr/cnt);
                if(!arrayList.isEmpty())
                    os.println("Median value: " + arrayList.get((arrayList.size()/2)));
                else
                    os.println("Median value: NaN");
                os.println("Nr. of values: " + cnt);
            }
            for(int o = 0; o < qualitativeKeys.length; o++){
                os.println("ID: " + qualitativeKeys[o]);
                String query = QueryBuilder.buildQueryForOneSpecification(qualitativeKeys[o]);
                ResultSet results = executeQuery(thisModel, query);
                int cnt = 0;
                while(results.hasNext()){
                    QuerySolution sol = results.next();
                    String literalValue = sol.getLiteral("value").getString().split("/")[0];
                    if(literalValue.contains("(")){
                        literalValue = literalValue.split("\\(")[1];
                        literalValue = literalValue.split("\\)")[0];
                    }
                    literalValue = literalValue.replace(",", ".");
                    if(!checkForCOMissingValues(literalValue)){
                        cnt++;
                    }
                }
                os.println("Nr. of values: " + cnt);
            }
        }
        try{
            os.close();
        }catch(Exception e){
            System.out.println("FAULT2");
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void calculateValuesVSO(){
        String[] keys = getKeys();
        String[] qualitativeKeys = getKeysForQualitative();
        PrintWriter os = null;
        try{
            os = new PrintWriter(new File("VSOValues.txt"));
        }catch(Exception e){
            System.out.println("FAULT");
            e.printStackTrace();
            System.exit(1);
        }
        for(String vsoModel: daimlerModels.keySet()){
            Model model = FileManager.get().loadModel(vsoModel);
            Map<String, PartialCar> currentModels = daimlerModels.get(vsoModel);
            for(PartialCar pc : currentModels.values()){
                os.println("Car modelname: " + pc.getModelName());
                for(int k = 0; k < keys.length; k++){
                    os.println("ID: " + keys[k]);
                    double nmbr = 0;
                    int cnt = 0;
                    ArrayList<Double> arrayList = new ArrayList<Double>();
                    ResultSet results = executeQuery(model, QueryBuilder.buildQueryForOneSpecificationVSOAndCOO(keys[k], pc.getLexRef(), null, false));
                    while(results.hasNext()){
                        QuerySolution qs = results.next();
                        String value = qs.getLiteral("vsoValue").getString();
                        cnt++;
                        nmbr += Double.parseDouble(value);
                        arrayList.add(Double.parseDouble(value));
                    }
                    os.println("Average value: " + nmbr/cnt);
                    os.println("Median value: " + arrayList.get((arrayList.size()/2)));
                    os.println("Nr. of values: " + cnt);
                }
                for(int o = 0; o < qualitativeKeys.length; o++){
                    os.println("ID: " + qualitativeKeys[o]);
                    int cnt = 0;
                    ResultSet results = executeQuery(model, QueryBuilder.buildQueryForOneSpecificationVSOAndCOO(qualitativeKeys[o], pc.getLexRef(), null, false));
                    while(results.hasNext()){
                        QuerySolution qs = results.next();
                        String value = qs.getLiteral("vsoValue").getString();
                        System.out.println(value);
                        cnt++;
                    }
                    os.println("Nr. of values: " + cnt);
                }
            }
        }
        try{
            os.close();
        }catch(Exception e){
            System.out.println("FAULT2");
            e.printStackTrace();
            System.exit(1);
        }

    }
}
