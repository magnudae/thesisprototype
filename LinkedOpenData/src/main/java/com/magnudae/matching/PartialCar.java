package com.magnudae.matching;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;


import com.hp.hpl.jena.rdf.model.Model;

public class PartialCar {
	private String modelName;
	private String lexRef;
	private Model lexicon;
	private String linkedConf = null;
	private List<Map<String, String>> validSpec;
	
	public PartialCar(String modelName, String lexRef, Model lexicon){
		this.modelName = modelName;
		this.lexRef = lexRef;
		this.lexicon = lexicon;
		validSpec = new LinkedList<Map<String, String>>();
	}
	
	public  List<Map<String, String>> getValidSpec(){
		return validSpec;
	}
	
	public void setValidSpec(Map<String, String> valid){
        validSpec.add(valid);
	}
	public String getModelName() {
		return modelName;
	}
	public String getLexRef() {
		return lexRef;
	}
	public Model getLexicon(){
		return lexicon;
	}
	public synchronized String getLinkedConf() {
		return linkedConf;
	}
	public synchronized void setLinkedConf(String linkedConf) {
		this.linkedConf = linkedConf;
	}
	
}
