package com.magnudae.matching;

/**
 * Created by magnudae on 1/13/14.
 */
public class QuantitativeValue extends Value {
    private String lower;
    private String  upper;

    public QuantitativeValue(String input){
        String[] splitted = input.split(",");
        this.lower = splitted[0];
        this.upper = splitted[1];
    }

    public double getLower(){
        if(lower.equals("-"))
            return 0.0;
        return Double.parseDouble(lower);
    }

    public double getUpper(){
        if(upper.equals("-"))
            return 1000000.0;
        return Double.parseDouble(upper);
    }

    public boolean isValid(String inputValue) {
        double parsedValue = Double.parseDouble(inputValue);
        return (this.getLower() <= parsedValue && this.getUpper() >= parsedValue);
    }

    public boolean isEmpty(){
        if(lower.equals("-") && upper.equals("-"))
            return true;
        else
            return false;
    }

    public String toString(){
        return lower + " - " + upper;
    }
}
