package com.magnudae.matching;

import java.util.Scanner;

public class Main {
	/**
	 * Can have option to only run VSO data, CO data or both
	 * @param args
	 */
	public static void main(String[] args) {

	}
	
	public static void specChooser(CarData cd, String option){
	/*	Scanner in = null;
		Scanner line = null;
		try{
			in = new Scanner(System.in);
			line = new Scanner(System.in);

		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
		
		printGuide();
		String wantedValue = "";
		int loop = -1;
		while(loop != 0){
			System.out.println("Choose your specification:");
			loop = in.nextInt();
			switch(loop){
			case(1): System.out.println("What fuel type do you want?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1628", wantedValue); 
					break;
			case(2): System.out.println("What kind of transmission?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1633", wantedValue); 
					break;
			case(3): System.out.println("How many seats?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1691", wantedValue); 
					break;
			case(4): System.out.println("Height?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT602", wantedValue); 
					break;
			case(5): System.out.println("Wheelbase?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1664", wantedValue); 
					break;
			case(6): System.out.println("Width?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT141", wantedValue); 
					break;
			case(7): System.out.println("Kerb weight (Excluding driver)?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1654", wantedValue); 
					break;
			case(8): System.out.println("Total possible weight?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1657", wantedValue); 
					break;
			case(9): System.out.println("Number of gears?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT47", wantedValue); 
					break;
			case(10): System.out.println("Acceleration?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1122", wantedValue); 
					break;
			case(11): System.out.println("Top speed?"); 
					wantedValue = line.nextLine();
				cd.searchThroughModels("PT1121", wantedValue); 
				break;
			case(12): System.out.println("Fuel consumption (combined)?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1643", wantedValue); 
					break;
			case(13): System.out.println("Fuel tank capacity(volume)?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT301", wantedValue); 
					break;
			case(14): System.out.println("Emission standard?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT3401", wantedValue); 
					break;
			case(15): System.out.println("Length?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1118", wantedValue); 
					break;
			case(16): System.out.println("CO2 emission g/km?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1640", wantedValue); 
					break;
			case(17): System.out.println("Nr. of doors?"); 
					wantedValue = line.nextLine();
					cd.searchThroughModels("PT1262", wantedValue); 
					break;
			case(18): cd.printCurrentModels(); break;
			case(19): cd = new CarData(option); break;
			case(0): break;
			default: printGuide();
			}
		}*/
		
	}
	
	public static void printGuide(){
		System.out.println("1. Fuel Type. Ex: Petrol, Diesel, Electric");
		System.out.println("2. Transmission. Ex: Manual, Automatic");
		System.out.println("3. Seating capacity. Ex: 4, 5");
		System.out.println("4. Car height");
		System.out.println("5. Wheelbase");
		System.out.println("6. Car width");
		System.out.println("7. Kerb weight (excluding driver)");
		System.out.println("8. Total possible weight");
		System.out.println("9. Number of gears");
		System.out.println("10. Acceleration from 0-100 km/h");
		System.out.println("11. Top speed");
		System.out.println("12. Fuel consumption (combined");
		System.out.println("13. Fuel tank capacity");
		System.out.println("14. EU Emission standard");
		System.out.println("15. Length");
		System.out.println("16. CO2 emission g/km");
		System.out.println("17. Nr. of doors");


		System.out.println("18. Show available models");
		System.out.println("19. Reset car chooser");
		System.out.println("0. Exit");
	}

}
