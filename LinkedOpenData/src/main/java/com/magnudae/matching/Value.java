package com.magnudae.matching;

/**
 * Created by magnudae on 1/17/14.
 */
public abstract class Value {


    public abstract boolean isValid(String inputValue);
    public abstract boolean isEmpty();
    public abstract String toString();
}
