package com.magnudae.matching;

/**
 * Created by magnudae on 1/17/14.
 */
public class QualitativeValue extends Value {
    private String value;


    public QualitativeValue(String input){
        value = input;
    }

    public String getValue(){
        return value;
    }

    public boolean isValid(String input){
        return input.equals(value);
    }

    @java.lang.Override
    public boolean isEmpty() {
        if(value.equals("")) return true;
        else return false;
    }

    public String toString(){
        return value;
    }
}
