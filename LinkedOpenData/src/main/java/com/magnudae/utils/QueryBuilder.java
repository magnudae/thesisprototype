package com.magnudae.utils;

import com.magnudae.controllers.FormBean;

import java.util.Map;
import java.util.List;
/**
 * Created by magnudae on 1/8/14.
 */
public class QueryBuilder {



    public static String buildQueryToSpecificationSearch(FormBean inputForm){
        StringBuilder sparqlQuery = new StringBuilder(Settings.SPARQLPREFIX + " SELECT ?modelURI ?value WHERE { ");
        String prefix = "?x";
       /* Map<String, String> quantitativeValues = inputForm.getQuantitativeValues();
        Map<String, String> qualitativeValues = inputForm.getQualitativeValues();

        int varCnt = 1;
        for(String key: quantitativeValues.keySet()){
            String valueVariable = prefix + "" + varCnt++;
            String value = quantitativeValues.get(key);
            if(!value.equals("")){
                sparqlQuery.append(" ?modelURI " + key + " [ gr:hasValue " + valueVariable +" ] . ");
                sparqlQuery.append(" FILTER (" + valueVariable +" = " + value + ") ");
            }
        }

        for(String key: qualitativeValues.keySet()){
            String valueVariable = prefix + "" + varCnt++;
            String value = qualitativeValues.get(key);
            if(!value.equals("")){
                sparqlQuery.append(" ?modelURI " + key + " [ gr:name " + valueVariable +" ] . ");
                sparqlQuery.append(" FILTER regex (" + valueVariable +", \"" + value + "\") ");
            }
        }

        sparqlQuery.append(" }");*/
        return sparqlQuery.toString();
    }


    public static String buildQueryForOneSpecification(String predicate){
        StringBuilder sparqlQuery = new StringBuilder(Settings.SPARQLPREFIX + " SELECT ?modelURI ?specification ?value WHERE { ");
        sparqlQuery.append(" ?modelURI " + predicate + " ?specification . ");
        sparqlQuery.append(" ?specification rdfs:label ?value . ");
        sparqlQuery.append(" }");

        return sparqlQuery.toString();
    }

    public static String buildQueryForOneSpecificationVSO(String predicate){
        StringBuilder sparqlQuery = new StringBuilder(Settings.SPARQLPREFIX + " SELECT ?modelURI ?specification ?vsoValue WHERE { ");
        sparqlQuery.append(" ?modelURI " + predicate + " ?specification . ");
        sparqlQuery.append(" { ?specification gr:hasValue ?vsoValue . }  ");
        sparqlQuery.append(" UNION { ?specification gr:name ?vsoValue . }  ");
        sparqlQuery.append(" }");

        return sparqlQuery.toString();
    }

    public static String buildQueryForOneSpecificationVSOAndCOO(String predicate, String modelURI, List<String> specsSoFar, boolean precision){
        StringBuilder sparqlQuery = new StringBuilder(Settings.SPARQLPREFIX + " SELECT ?specification ?vsoValue WHERE { ");
        sparqlQuery.append("<" +modelURI + "> " + predicate + " ?specification . ");
        sparqlQuery.append(" { ?specification gr:hasValue ?vsoValue . }  ");
        sparqlQuery.append(" UNION { ?specification gr:name ?vsoValue . }  ");
        if(precision){
            if(!specsSoFar.isEmpty()){
                sparqlQuery.append(" FILTER NOT EXISTS  { ");
                for(String spec :specsSoFar){
                    sparqlQuery.append(" ?specification coo:incompatibleWith <" + spec + "> . ");
                }
                sparqlQuery.append(" }");
            }
        }
        sparqlQuery.append(" }");
        return sparqlQuery.toString();
    }
}
