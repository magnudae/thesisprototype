package com.magnudae.utils;

public class Namespaces {
	public static final String VSO = "http://purl.org/vso/ns#";
	public static final String COO = "http://purl.org/coo/ns#";
	public static final String CO = "http://purl.org/configurationontology#";
	public static final String DAIM = "http://folk.uio.no/magnudae/daimler#";
	public static final String GR = "http://purl.org/goodrelations/v1#";

    public static final String FUELTYPE = "vso:fuelType";
    public static final String TRANSMISSION = "vso:transmission";
    public static final String DOORS = "vso:doors";
    public static final String LENGTH = "vso:length";
    public static final String EMISSION = "daim:emission";
    public static final String EMISSIONSTANDARD = "vso:meetsEmissionStandard";
    public static final String WIDTH = "vso:width";
    public static final String WEIGHT = "vso:weight";
    public static final String WEIGHTTOTAL = "vso:weightTotal";
    public static final String HEIGTH = "vso:height";
    public static final String GEARSTOTAL = "vso:gearsTotal";
    public static final String SEATS = "vso:seatingCapacity";
    public static final String FUELTANKVOLUME = "vso:fuelTankVolume";
    public static final String WHEELBASE = "vso:wheelbase";
    public static final String FUELCONSUMPTION = "vso:fuelConsumption";
    public static final String ACCELERATION = "vso:acceleration";
    public static final String SPEED = "vso:speed";


    public static final String XSDINT = "http://www.w3.org/2001/XMLSchema#int";





}
