package com.magnudae.utils;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.magnudae.matching.PartialCar;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.FileManager;

public class Settings {
    /*
     * Pre-computed use case settings
     */
    public static String[] daimlerLexicons = {"http://folk.uio.no/magnudae/LinkedOpenData/daimlerVsoFictive.ttl", "http://folk.uio.no/magnudae/LinkedOpenData/daimlerVsoFictive2.ttl"};
    public static String compatibilityCOO = "http://folk.uio.no/magnudae/LinkedOpenData/compatibilityTriples.ttl";
    public static String localCOO = "/ifi/asgard/m00/magnudae/Downloads/NS/COO.owl";
    public static String renaultStartingPoint = "http://uk.co.rplug.renault.com/docs#this";
    public static List<String> renaultLex = new LinkedList<String>();
    public static final String SPARQLPREFIX = "PREFIX co: <http://purl.org/configurationontology#> " +
            "PREFIX coo: <http://purl.org/coo/ns#> " +
            "PREFIX vso: <http://purl.org/vso/ns#> " +
            "PREFIX rdfs:	<http://www.w3.org/2000/01/rdf-schema#> " +
            "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
            "PREFIX daim: <http://folk.uio.no/magnudae/daimler#>";

    //Key: Lexicon URI, value: Model names
    public static Map<String, String> renaultModelNames = new HashMap<String, String>();

    // Key: Lexicon URI, value: Aligned lexicon path
    public static Map<String, String> renaultLexicons = new HashMap<String, String>();

    public static void initModelNames(){
        Model model = FileManager.get().loadModel(renaultStartingPoint);
        String sparqlQuery = SPARQLPREFIX + " SELECT ?lex ?modelName " +
                "WHERE { " +
                "?model co:linkedConf ?linked . " +
                "?model rdfs:label ?modelName . " +
                "?linked co:lexicon ?lex . " +
                "}";

        Query query = QueryFactory.create(sparqlQuery);
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        ResultSet rs = qe.execSelect();
        while(rs.hasNext()){
            QuerySolution qs = rs.nextSolution();
            renaultModelNames.put(qs.get("lex").toString(), qs.getLiteral("modelName").getString());
        }
    }

    public static void initLexicons(){
        String location = "/ifi/asgard/m00/magnudae/UniOppgaver/Master/Prototype/LinkedOpenData/";
        String prefix = "lexicon";
        String suffix = ".ttl";
        int i = 1;

        while(true){
            String tmp = location +"" + prefix +"" + i + "" + suffix;
            Model model;
            try{
                model = FileManager.get().loadModel(tmp);
            }catch(RuntimeException e){
                System.out.println("No more lexicons to read");
                return;
            }

            String sparqlQuery = SPARQLPREFIX + " SELECT ?lex " +
                    "WHERE { " +
                    "?var co:lexicon ?lex . " +
                    "}";
            Query query = QueryFactory.create(sparqlQuery);
            QueryExecution qe = QueryExecutionFactory.create(query, model);
            ResultSet rs = qe.execSelect();
            while(rs.hasNext()){
                QuerySolution qs = rs.nextSolution();
                renaultLexicons.put(qs.get("lex").toString(), tmp);
                break;
            }

            i++;
        }

    }
    public static void updateRenaultLexicons(){
        Model m = FileManager.get().loadModel(renaultStartingPoint);
        String sparqlQuery = SPARQLPREFIX + " SELECT ?lex ?modelName " +
                "WHERE { " +
                "?model co:linkedConf ?linked . " +
                "?model rdfs:label ?modelName . " +
                "?linked co:lexicon ?lex . " +
                "}";
        Query query = QueryFactory.create(sparqlQuery);
        QueryExecution qe = QueryExecutionFactory.create(query, m);
        ResultSet rs = qe.execSelect();
        int i = 1;
        while(rs.hasNext()){
            QuerySolution qs = rs.nextSolution();
            String lexicon = qs.get("lex").toString();
            //Hardcoded contruct, deal with it!
            System.out.println(lexicon);
            String lexiconFileName = "lexicon" + i + ".ttl";
            renaultLexicons.put(lexicon, lexiconFileName);
            Model lexiconModel = FileManager.get().loadModel(lexicon);

            Query w = QueryFactory.read("/ifi/asgard/m00/magnudae/UniOppgaver/Master/Notater/research/matching/lexiconConstruct.rq");
            Model constructed1 = QueryExecutionFactory.create(w, lexiconModel).execConstruct();

            Query w2 = QueryFactory.read("/ifi/asgard/m00/magnudae/UniOppgaver/Master/Notater/research/matching/lexiconConstructPart2.rq");
            Model constructed2 = QueryExecutionFactory.create(w2, lexiconModel).execConstruct();

            lexiconModel.add(constructed1);
            lexiconModel.add(constructed2);

            OutputStream os = null;

            try{
                os = new FileOutputStream(new File(lexiconFileName));
            }catch(Exception e){
                System.out.println("FAULT");
                e.printStackTrace();
                System.exit(1);
            }
            lexiconModel.write(os, "TURTLE");

            try{
                os.close();
            }catch(Exception e){
                System.out.println("FAULT2");
                e.printStackTrace();
                System.exit(1);
            }
            i++;
        }
    }

}
