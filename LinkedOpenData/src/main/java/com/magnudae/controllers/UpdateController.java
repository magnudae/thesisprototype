package com.magnudae.controllers;

import com.magnudae.matching.CarData;
import com.magnudae.controllers.FormBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import javax.servlet.http.HttpServletRequest;



import com.magnudae.utils.Settings;
/**
 * Created by magnudae on 1/7/14.
 */
@Controller
public class UpdateController {

    @RequestMapping(value="/update", method = RequestMethod.POST)
    public String index() {
        Settings.updateRenaultLexicons();
        return "redirect:/index";
    }

    @RequestMapping(value="/update", method = RequestMethod.GET)
    public String reset() {
        return "redirect:/index";
    }

    @RequestMapping(value="/test", method = RequestMethod.GET)
    public String test(HttpServletRequest request) {
        String s = request.getQueryString();
        CarData c = new CarData("-adsada", true);
        c.calculateValuesCO();
        c.calculateValuesVSO();
/*
        FormBean fb = new FormBean();
        fb.setAcceleration("1,100000");
        fb.setDoors("1,10");
        fb.setEmission("1,1000");
        fb.setEmissionStandard("Euro 5");
        fb.setFuelConsumption("1,1000");
        fb.setFuelTankVolume("1,1000");
        fb.setFuelType("Diesel");
        fb.setGearsTotal("1,1000");
        fb.setHeight("1,20000");
        fb.setLength("1,20000");
        fb.setSeats("1,10");
        fb.setTopspeed("1,40000");
        fb.setTransmission("Manual Gearbox");
        fb.setWeight("1, 20000");
        fb.setWeightTotal("1, 100000");
        fb.setWheelbase("1,30000");
        fb.setWidth("1, 100000");
        CarData c;
        if(s.contains("true"))
            c = new CarData("-asdasd", true);
        else
            c = new CarData("-asdasd", false);
        c.searchThroughModels(fb);
        c.printCurrentModels();*/
        return "redirect:/index";
    }
}
