package com.magnudae.controllers;
import com.magnudae.matching.CarData;
import com.magnudae.matching.PartialCar;
import com.magnudae.controllers.FormBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import java.util.*;
import com.magnudae.utils.Namespaces;
import com.magnudae.utils.Settings;
import com.magnudae.utils.QueryBuilder;
import javax.servlet.http.HttpServletRequest;




@Controller
@RequestMapping("/index")
@SessionAttributes("formBean")
public class IndexController {
    CarData cd;

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap  map) {
        cd = new CarData("-vsos", false);
        map.addAttribute("message", "Car chooser 3000");
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String sumbit(FormBean form,  ModelMap map) {
        cd.setHighPrecision(form.getPrecision());
        cd.enableOntologies(form.getOntology());
        cd.searchThroughModels(form);
        map.addAttribute("message", "Thanks for picking a car");
        map.addAttribute("modelList", cd.getCurrentModels());
        //cd.printCurrentModels();
        return "index";
    }


    @ModelAttribute("formBean")
    public FormBean createFormBean() {
        return new FormBean();
    }
}