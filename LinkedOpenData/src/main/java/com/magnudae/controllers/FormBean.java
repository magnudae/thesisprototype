package com.magnudae.controllers;

import java.util.HashMap;
import java.util.Map;

import com.magnudae.matching.QuantitativeValue;
import com.magnudae.matching.QualitativeValue;
import com.magnudae.matching.Value;
import com.magnudae.utils.Namespaces;



public class FormBean {

    private Map<String, Value> values = new HashMap<String, Value>();
    private String ontology;
    private boolean precision;
    private boolean order;


    public void setOntology(String ontology){
        this.ontology = ontology;
    }

    public void setPrecision(String precision){
        if(precision.equals("true"))
            this.precision = true;
        else
            this.precision = false;
    }

    public void setOrder(boolean order){
            this.order =order;
    }
    public void setHeight(String height) {
        values.put(Namespaces.HEIGTH, new QuantitativeValue(height));
    }

    public void setWheelbase(String wheelbase) {
        values.put(Namespaces.WHEELBASE, new QuantitativeValue(wheelbase));
    }

    public void setWidth(String width) {
        values.put(Namespaces.WIDTH, new QuantitativeValue(width));
    }

    public void setWeight(String weight) {
        values.put(Namespaces.WEIGHT, new QuantitativeValue(weight));
    }

    public void setWeightTotal(String weightTotal) {
        values.put(Namespaces.WEIGHTTOTAL, new QuantitativeValue(weightTotal));
    }

    public void setGearsTotal(String gearsTotal) {
        values.put(Namespaces.GEARSTOTAL, new QuantitativeValue(gearsTotal));
    }

    public void setSeats(String seats) {
        values.put(Namespaces.SEATS, new QuantitativeValue(seats));
    }

    public void setAcceleration(String acceleration) {
        values.put(Namespaces.ACCELERATION, new QuantitativeValue(acceleration));
    }

    public void setTopspeed(String topspeed) {
        values.put(Namespaces.SPEED, new QuantitativeValue(topspeed));
    }

    public void setFuelConsumption(String fuelConsumption) {
        values.put(Namespaces.FUELCONSUMPTION, new QuantitativeValue(fuelConsumption));
    }

    public void setFuelTankVolume(String fuelTankVolume) {
        values.put(Namespaces.FUELTANKVOLUME, new QuantitativeValue(fuelTankVolume));
    }

    public void setEmissionStandard(String emissionStandard) {
        values.put(Namespaces.EMISSIONSTANDARD, new QualitativeValue(emissionStandard));
    }

    public void setLength(String length) {
        values.put(Namespaces.LENGTH, new QuantitativeValue(length));
    }

    public void setDoors(String doors) {
        values.put(Namespaces.DOORS, new QuantitativeValue(doors));
    }

    public void setEmission(String emission) {
        values.put(Namespaces.EMISSION, new QuantitativeValue(emission));
    }


    public Map<String, Value> getValues(){
        return values;
    }

    public boolean getPrecision(){ return precision; }
    public String getOntology(){
        return ontology;
    }
    public boolean getOrder(){
        return order;
    }


    public void setFuelType(String fuelType){
        values.put(Namespaces.FUELTYPE, new QualitativeValue(fuelType) );
    }

    public void setTransmission(String transmission){
        values.put(Namespaces.TRANSMISSION, new QualitativeValue(transmission));
    }

}
