
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="top.jsp" />

<html>
   <body>
   <h2 class="container">${message}</h2>
        <div class="container">
            <form id="dataForm" method="POST" action="<c:url value="index" />" modelAttribute="formBean">
                <div class="col-md-4 row">
                    <label class="col-md-5" >Fuel type</label>
                    <select name="fuelType">
                        <option value="" selected="selected">-</option>
                        <option value="Diesel">Diesel</option>
                        <option value="Unleaded Petrol">Unleaded Petrol</option>
                        <option value="Electricity">Electricity</option>
                    </select>
                    <!--input class="col-md-5" type="select" name="fuelType" / -->
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" > Transmission type</label>
                    <select name="transmission">
                        <option value="" selected="selected">-</option>
                        <option value="Automatic Gearbox">Automatic</option>
                        <option value="Manual Gearbox">Manual</option>
                        <option value="Direct Drive">Direct Drive(electric)</option>
                    </select>
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" > Car height</label>
                    <input class="col-md-2" type="text" name="height" value="-" />
                    <input class="col-md-2" type="text" name="height" value="-" />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" > Car length</label>
                    <input class="col-md-2" type="text" name="length" value="-" />
                    <input class="col-md-2" type="text" name="length" value="-"  />

                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" > Car width</label>
                    <input class="col-md-2" type="text" name="width" value="-"  />
                    <input class="col-md-2" type="text" name="width" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Curb weight</label>
                    <input class="col-md-2" type="text" name="weight" value="-"  />
                    <input class="col-md-2" type="text" name="weight" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Max weight</label>
                    <input class="col-md-2" type="text" name="weightToal" value="-"  />
                    <input class="col-md-2" type="text" name="weightToal" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Nr. of gears</label>
                    <select name="gearsTotal">
                        <option value="-" selected="selected">-</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                    <select name="gearsTotal">
                        <option value="-" selected="selected">-</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Seating capacity</label>
                    <select name="seats">
                        <option value="-" selected="selected">-</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                    <select name="seats">
                        <option value="-" selected="selected">-</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Acceleration</label>
                    <input class="col-md-2" type="text" name="acceleration" value="-"  />
                    <input class="col-md-2" type="text" name="acceleration" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Top speed</label>
                    <input class="col-md-2" type="text" name="topspeed" value="-"  />
                    <input class="col-md-2" type="text" name="topspeed" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Fuel consumption</label>
                    <input class="col-md-2" type="text" name="fuelConsumption" value="-"  />
                    <input class="col-md-2" type="text" name="fuelConsumption" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Fuel tank capacity</label>
                    <input class="col-md-2" type="text" name="fuelTankVolume" value="-"  />
                    <input class="col-md-2" type="text" name="fuelTankVolume" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Euro emission standard</label>
                    <select name="emissionStandard">
                        <option value="" selected="selected">-</option>
                        <option value="Euro 1">Euro 1</option>
                        <option value="Euro 2">Euro 2</option>
                        <option value="Euro 3">Euro 3</option>
                        <option value="Euro 4">Euro 4</option>
                        <option value="Euro 5">Euro 5</option>
                        <option value="Euro 6">Euro 6</option>
                    </select>
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5" >Nr. of doors</label>
                    <input class="col-md-2" type="text" name="doors" value="-"  />
                    <input class="col-md-2" type="text" name="doors" value="-"  />
                </div>
                <div class="col-md-4 row">
                    <label class="col-md-5"> CO2 emission(g/km)</label>
                    <input class="col-md-2" type="text" name="emission" value="-"  />
                    <input class="col-md-2" type="text" name="emission" value="-"  />
                </div>
                <div class="container col-md-12">
                    <label>Ontology to use</label>
                    <select name="ontology">
                        <option value="-vso">COO/VSO</option>
                        <option value="-co">CO</option>
                        <option value="-care" selected="selected">Both</option>
                    </select>
                    <label>High/low precision</label>
                    <select name="precision">
                        <option value="true">High</option>
                        <option value="false" selected="selected">low</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <button type="sumbit" class="sumbitButton">Sumbit</button>
                </div>
            </form>
        </div>
        <div class="container">
            <form method="POST" action="<c:url value="update" />">
                <div class="col-md-8">
                    <button type="sumbit" class="sumbitButton">Update Renault information</button>
                </div>
            </form>
        </div>
        <div class="container">
                <button onclick="location.href='http://localhost:8080/linkedopendata-magnudae/update'">Reset</button>
        </div>
        <div class="container">
            <table>
                <c:forEach items="${modelList}" var="model">
                   <tr>
                      <td>${model}</td>
                   </tr>
                </c:forEach>
              </table>
        </div>
   </body>
</html>
