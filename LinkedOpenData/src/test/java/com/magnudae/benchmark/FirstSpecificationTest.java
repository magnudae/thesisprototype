package com.magnudae.benchmark;

import static org.junit.Assert.assertEquals;

import com.magnudae.controllers.FormBean;
import com.magnudae.matching.CarData;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkMethodChart;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkHistoryChart;
import com.carrotsearch.junitbenchmarks.annotation.AxisRange;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.annotation.LabelType;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;

@BenchmarkOptions(benchmarkRounds = 5, warmupRounds = 0)
@AxisRange(min = 0, max = 100)
@BenchmarkMethodChart(filePrefix = "firstSpecificationFound_chart")
@RunWith(JUnit4.class)
public class FirstSpecificationTest {

    private CarData searchModule;
    private static FormBean testFormSmall;
    private static FormBean testFormLarge;
    
    @Rule
    public TestRule benchmarkRun = new BenchmarkRule();

    @Before
    public void prepareCarData(){
        searchModule = new CarData("asdhasd", false);
        }

    @BeforeClass
    public static void prepare()
    {
        testFormSmall = new FormBean();
        testFormSmall.setFuelType("Diesel");
        testFormSmall.setTransmission("Manual Gearbox");
        testFormSmall.setSeats("4,7");
        testFormSmall.setTopspeed("190,-");
        testFormSmall.setAcceleration("-,-");
        testFormSmall.setDoors("-,-");
        testFormSmall.setEmission("-,-");
        testFormSmall.setEmissionStandard("");
        testFormSmall.setFuelConsumption("-,-");
        testFormSmall.setFuelTankVolume("-,-");
        testFormSmall.setGearsTotal("-,-");
        testFormSmall.setHeight("-,-");
        testFormSmall.setLength("-,-");
        testFormSmall.setWeight("-,-");
        testFormSmall.setWeightTotal("-,-");
        testFormSmall.setWheelbase("-,-");
        testFormSmall.setWidth("-,-");

        testFormLarge = new FormBean();
        testFormLarge.setAcceleration("1,100000");
        testFormLarge.setDoors("1,10");
        testFormLarge.setEmission("1,1000");
        testFormLarge.setEmissionStandard("Euro 5");
        testFormLarge.setFuelConsumption("1,1000");
        testFormLarge.setFuelTankVolume("1,1000");
        testFormLarge.setFuelType("Diesel");
        testFormLarge.setGearsTotal("1,1000");
        testFormLarge.setHeight("1,20000");
        testFormLarge.setLength("1,20000");
        testFormLarge.setSeats("1,10");
        testFormLarge.setTopspeed("1,40000");
        testFormLarge.setTransmission("Manual Gearbox");
        testFormLarge.setWeight("1, 20000");
        testFormLarge.setWeightTotal("1, 100000");
        testFormLarge.setWheelbase("1,30000");
        testFormLarge.setWidth("1, 100000");
    }

    @Ignore
    @Test
    public void timeToFindFirstSpecificationSmallVSOTest(){
        searchModule.vso = true;
        searchModule.co = false;
        searchModule.findOneSpec = true;
        searchModule.searchThroughModels(testFormSmall);
    }
    @Ignore
    @Test
    public void timeToFindFirstSpecificationSmallCOTest() {
        searchModule.vso = false;
        searchModule.co = true;
        searchModule.findOneSpec = true;
        searchModule.searchThroughModels(testFormSmall);
    }

    @Ignore
    @Test
    public void timeToFindFirstSpecificationLargeVSOTest(){
        searchModule.vso = true;
        searchModule.co = false;
        searchModule.findOneSpec = true;
        searchModule.searchThroughModels(testFormLarge);
    }
    @Ignore
    @Test
    public void timeToFindFirstSpecificationLargeCOTest() {
        searchModule.vso = false;
        searchModule.co = true;
        searchModule.findOneSpec = true;
        searchModule.searchThroughModels(testFormLarge);
    }
}
